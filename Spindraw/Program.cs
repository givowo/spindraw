﻿using FFMediaToolkit.Audio;
using FFMediaToolkit.Decoding;
using FFMediaToolkit.Encoding;
using FFMediaToolkit.Graphics;
using FFMpegCore;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Generic;

namespace Spindraw
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Interoperability", "CA1416:Validate platform compatibility", Justification = "<Pending>")]
    class Program
    {
        Random rand = Random.Shared;
        static string videoName = "";

        Bitmap spindaBase;
        Bitmap spindaMask;
        Bitmap spotTL;
        Bitmap spotTR;
        Bitmap spotBL;
        Bitmap spotBR;

        int headWidth = 37;
        int headHeight = 33;
        int headOverlapX = 10;
        int headOverlapY = 5;

        int bodyHeight = 49;

        int spriteBorderX = 8;
        int spriteBorderY = 8;

        int spotTLX = 0;
        int spotTLY = 0;
        int spotTRX = 24;
        int spotTRY = 1;
        int spotBLX = 6;
        int spotBLY = 18;
        int spotBRX = 18;
        int spotBRY = 19;

        // To prevent re-allocation
        int frameProcessing = 0;
        Bitmap frame;
        Bitmap spindaFrame;
        int horizontalSpindas;
        int paddingX;
        int verticalSpindas;
        int paddingY;

        static void Main(string[] args)
        {
            string vid = @"evenSmaller.mp4";
            if (args.Length == 0)
            {
                Console.WriteLine("No video given! Drag one onto the exe");
                Console.WriteLine("Press any key to close this window");
                Console.ReadKey();
                return;
            }

            vid = args[0];
            videoName = vid.Split(@"\")[vid.Split(@"\").Length - 1].Split(".")[0];
            Program spindraw = new Program();
            spindraw.Draw(vid);
        }

        void Draw(string vid)
        {
            if (Directory.Exists(@".\frames")) Directory.Delete(@".\frames", true);
            if (Directory.Exists(@".\spinda_frames")) Directory.Delete(@".\spinda_frames", true);
            Directory.CreateDirectory(@".\frames");
            Directory.CreateDirectory(@".\spinda_frames");

            Assembly asm = Assembly.GetExecutingAssembly();
            spindaBase = (Bitmap)Image.FromStream(asm.GetManifestResourceStream("Spindraw.Blank_Spinda.png"));
            spindaMask = (Bitmap)Image.FromStream(asm.GetManifestResourceStream("Spindraw.Mask_Spinda.png"));
            spotTL = (Bitmap)Image.FromStream(asm.GetManifestResourceStream("Spindraw.SpotTL.png"));
            spotTR = (Bitmap)Image.FromStream(asm.GetManifestResourceStream("Spindraw.SpotTR.png"));
            spotBL = (Bitmap)Image.FromStream(asm.GetManifestResourceStream("Spindraw.SpotBL.png"));
            spotBR = (Bitmap)Image.FromStream(asm.GetManifestResourceStream("Spindraw.SpotBR.png"));

            frameProcessing = 0;
            FFMediaToolkit.FFmpegLoader.FFmpegPath = @".\";
            var file = MediaFile.Open(vid);
            List<string> allImages = new List<string>(0);

            while (file.Video.TryGetNextFrame(out ImageData imageData))
            {
                //Console.WriteLine($"Processing frame {frameProcessing}/{file.Video.Info.NumberOfFrames}");

                frame = ToBitmap(imageData);
                spindaFrame = new Bitmap(frame.Width, frame.Height);

                horizontalSpindas = frame.Width / (headWidth - headOverlapX);
                paddingX = frame.Width - horizontalSpindas * (headWidth - headOverlapX);
                verticalSpindas = frame.Height / (headHeight - headOverlapY);
                paddingY = frame.Height - verticalSpindas * (headHeight - headOverlapY);

                for (int y = 0; y < verticalSpindas; y++)
                {
                    for (int x = 0; x < horizontalSpindas; x++)
                    {
                        DoHead(x * (headWidth - headOverlapX) + paddingX / 2, y * (headHeight - headOverlapY));
                        //Console.WriteLine($"Processing frame {frameProcessing} {y * horizontalSpindas + x}/{verticalSpindas * horizontalSpindas}");
                    }
                }

                //frame.Save($@".\frames\frame_{frameProcessing}.png");
                spindaFrame.Save($@".\spinda_frames\{frameProcessing}.png");
                //Process.Start("explorer.exe", $@".\spinda_frames\frame_{frameProcessing}.png");

                frame.Dispose();
                spindaFrame.Dispose();
                frameProcessing++;

                Console.WriteLine($"{(((float)frameProcessing/ (float)file.Video.Info.NumberOfFrames) * 100).ToString("0")}%\t{frameProcessing}/{file.Video.Info.NumberOfFrames}");
            }

            Console.WriteLine("Combining video and audio");

            foreach (string spindaframefile in Directory.GetFiles($@".\spinda_frames"))
            {
                allImages.Add(spindaframefile);
            }

            FFMpegArguments
            .FromFileInput($@".\spinda_frames\%d.png", false, (Action<FFMpegArgumentOptions>)(options => options.WithFramerate(60)))
            .OutputToFile(@$".\{videoName}_spinda_video.mp4", addArguments: (Action<FFMpegArgumentOptions>)(options => options
            .WithFrameOutputCount(allImages.Count)))
            .ProcessSynchronously();

            FFMpeg.ExtractAudio(vid, Directory.GetCurrentDirectory() + @$"\{videoName}_spinda_audio.mp3");
            //FFMpeg.JoinImageSequence(Directory.GetCurrentDirectory() + @$"\{videoName}_spinda_video.mp4", file.Video.Info.AvgFrameRate, allImages.ToArray());
            FFMpeg.ReplaceAudio(Directory.GetCurrentDirectory() + @$"\{videoName}_spinda_video.mp4", Directory.GetCurrentDirectory() + @$"\{videoName}_spinda_audio.mp3", Directory.GetCurrentDirectory() + @$"\{videoName}_spinda.mp4");

            File.Delete(Directory.GetCurrentDirectory() + @$"\{videoName}_spinda_audio.mp3");
            File.Delete(Directory.GetCurrentDirectory() + @$"\{videoName}_spinda_video.mp4");

            if (Directory.Exists(@".\frames")) Directory.Delete(@".\frames", true);
            if (Directory.Exists(@".\spinda_frames")) Directory.Delete(@".\spinda_frames", true);

            Console.WriteLine("Done!");
            Console.WriteLine($@"Output video is {videoName}_spinda.mp4");
            Console.WriteLine("Press any key to close this window");
            Console.ReadKey();
        }

        void SplitHead(int minX, int maxX, int minY, int maxY)
        {
            for (int y = minY; y < maxY; y++)
            {
                for (int x = minX; x < maxX; x++)
                {
                    DoHead(x * (headWidth - headOverlapX) + paddingX / 2, y * (headHeight - headOverlapY));
                    Console.WriteLine($"Processing frame {frameProcessing} {y * horizontalSpindas + x}/{verticalSpindas * horizontalSpindas}");
                }
            }
        }

        void DoHead(int cornerX, int cornerY)
        {
            Bitmap spinda;
            int spotTLPosX = 8;
            int spotTLPosY = 8;
            int spotTRPosX = 8;
            int spotTRPosY = 8;
            int spotBLPosX = 8;
            int spotBLPosY = 8;
            int spotBRPosX = 8;
            int spotBRPosY = 8;
            int spotPosX = 0;
            int spotPosY = 0;
            Color maskedSpot;
            Color spindaFrameColor;
            Color spindaSpriteColor;
            float bestIntensityTL = 0;
            float bestIntensityTR = 0;
            float bestIntensityBL = 0;
            float bestIntensityBR = 0;
            float currentIntensityTL = 0;
            float currentIntensityTR = 0;
            float currentIntensityBL = 0;
            float currentIntensityBR = 0;

            spinda = spindaBase.Clone(new RectangleF(0, 0, spindaBase.Width, spindaBase.Height), spindaBase.PixelFormat);

            bestIntensityTL = 0;
            bestIntensityTR = 0;
            bestIntensityBL = 0;
            bestIntensityBR = 0;

            spotTLPosX = 0;
            spotTLPosY = 0;
            spotTRPosX = 15;
            spotTRPosY = 0;
            spotBLPosX = 0;
            spotBLPosY = 15;
            spotBRPosX = 15;
            spotBRPosY = 15;

            // this code sucks but this ESPECIALLY sucks
            for (int y = 0; y < 15; y++)
            {
                for (int x = 0; x < 15; x++)
                {
                    currentIntensityTL = 0;
                    currentIntensityTR = 0;
                    currentIntensityBL = 0;
                    currentIntensityBR = 0;

                    for (int y2 = 0; y2 < spotTL.Height; y2++)
                    {
                        for (int x2 = 0; x2 < spotTL.Width; x2++)
                        {
                            if (cornerY + spotTLY + y + y2 < 0 || cornerY + spotTLY + y + y2 >= frame.Height || cornerX + spotTLX + x + x2 < 0 || cornerX + spotTLX + x + x2 >= frame.Width) continue;
                            currentIntensityTL += Intensity(frame.GetPixel(cornerX + spotTLX + x + x2, cornerY + spotTLY + y + y2));
                        }
                    }

                    if (currentIntensityTL > bestIntensityTL)
                    {
                        bestIntensityTL = currentIntensityTL;
                        spotTLPosX = x;
                        spotTLPosY = y;
                    }

                    for (int y2 = 0; y2 < spotTR.Height; y2++)
                    {
                        for (int x2 = 0; x2 < spotTR.Width; x2++)
                        {
                            if (cornerY + spotTRY + y + y2 < 0 || cornerY + spotTRY + y + y2 >= frame.Height || cornerX + spotTRX + x + x2 < 0 || cornerX + spotTRX + x + x2 >= frame.Width) continue;
                            currentIntensityTR += Intensity(frame.GetPixel(cornerX + spotTRX + x + x2, cornerY + spotTRY + y + y2));
                        }
                    }

                    if (currentIntensityTR > bestIntensityTR)
                    {
                        bestIntensityTR = currentIntensityTR;
                        spotTRPosX = x;
                        spotTRPosY = y;
                    }

                    for (int y2 = 0; y2 < spotBL.Height; y2++)
                    {
                        for (int x2 = 0; x2 < spotBL.Width; x2++)
                        {
                            if (cornerY + spotBLY + y + y2 < 0 || cornerY + spotBLY + y + y2 >= frame.Height || cornerX + spotBLX + x + x2 < 0 || cornerX + spotBLX + x + x2 >= frame.Width) continue;
                            currentIntensityBL += Intensity(frame.GetPixel(cornerX + spotBLX + x + x2, cornerY + spotBLY + y + y2));
                        }
                    }

                    if (currentIntensityBL > bestIntensityBL)
                    {
                        bestIntensityBL = currentIntensityBL;
                        spotBLPosX = x;
                        spotBLPosY = y;
                    }

                    for (int y2 = 0; y2 < spotBR.Height; y2++)
                    {
                        for (int x2 = 0; x2 < spotBR.Width; x2++)
                        {
                            if (cornerY + spotBRY + y + y2 < 0 || cornerY + spotBRY + y + y2 >= frame.Height || cornerX + spotBRX + x + x2 < 0 || cornerX + spotBRX + x + x2 >= frame.Width) continue;
                            currentIntensityBR += Intensity(frame.GetPixel(cornerX + spotBRX + x + x2, cornerY + spotBRY + y + y2));
                        }
                    }

                    if (currentIntensityBR > bestIntensityBR)
                    {
                        bestIntensityBR = currentIntensityBR;
                        spotBRPosX = x;
                        spotBRPosY = y;
                    }
                }
            }

            spotPosX = spotTLX + spotTLPosX;
            spotPosY = spotTLY + spotTLPosY;
            for (int y = 0; y < spotTL.Height; y++)
            {
                for (int x = 0; x < spotTL.Width; x++)
                {
                    maskedSpot = Multiply(spindaMask.GetPixel(spotPosX + x, spotPosY + y), spotTL.GetPixel(x, y));
                    spinda.SetPixel(spotPosX + x, spotPosY + y, Overlap(maskedSpot, spinda.GetPixel(spotPosX + x, spotPosY + y)));
                }
            }

            spotPosX = spotTRX + spotTRPosX;
            spotPosY = spotTRY + spotTRPosY;
            for (int y = 0; y < spotTR.Height; y++)
            {
                for (int x = 0; x < spotTR.Width; x++)
                {
                    maskedSpot = Multiply(spindaMask.GetPixel(spotPosX + x, spotPosY + y), spotTR.GetPixel(x, y));
                    spinda.SetPixel(spotPosX + x, spotPosY + y, Overlap(maskedSpot, spinda.GetPixel(spotPosX + x, spotPosY + y)));
                }
            }

            spotPosX = spotBLX + spotBLPosX;
            spotPosY = spotBLY + spotBLPosY;
            for (int y = 0; y < spotBL.Height; y++)
            {
                for (int x = 0; x < spotBL.Width; x++)
                {
                    maskedSpot = Multiply(spindaMask.GetPixel(spotPosX + x, spotPosY + y), spotBL.GetPixel(x, y));
                    spinda.SetPixel(spotPosX + x, spotPosY + y, Overlap(maskedSpot, spinda.GetPixel(spotPosX + x, spotPosY + y)));
                }
            }

            spotPosX = spotBRX + spotBRPosX;
            spotPosY = spotBRY + spotBRPosY;
            for (int y = 0; y < spotBR.Height; y++)
            {
                for (int x = 0; x < spotBR.Width; x++)
                {
                    maskedSpot = Multiply(spindaMask.GetPixel(spotPosX + x, spotPosY + y), spotBR.GetPixel(x, y));
                    spinda.SetPixel(spotPosX + x, spotPosY + y, Overlap(maskedSpot, spinda.GetPixel(spotPosX + x, spotPosY + y)));
                }
            }

            int frameY = 0;
            int frameX = 0;
            for (int y = 0; y < bodyHeight; y++)
            {
                frameY = y + cornerY;
                if (frameY >= frame.Height) break;

                for (int x = 0; x < headWidth; x++)
                {
                    frameX = x + cornerX;
                    if (frameX >= frame.Width) break;

                    spindaFrameColor = spindaFrame.GetPixel(frameX, frameY);
                    spindaSpriteColor = spinda.GetPixel(spriteBorderX + x, spriteBorderY + y);

                    spindaFrame.SetPixel(frameX, frameY, Overlap(spindaSpriteColor, spindaFrameColor));
                }
            }

            spinda.Dispose();
        }

        float Intensity(Color a)
        {
            float alpha = a.A / 255f;
            return (a.R * 0.2126f * alpha + a.G * 0.7152f * alpha + a.B * 0.0722f * alpha);
        }

        Color Overlap(Color a, Color b)
        {
            float alpha = a.A / 255f;
            float blpha = 1f - alpha;

            return Color.FromArgb(Math.Clamp(a.A + b.A, 0, 255), (int)(a.R * alpha + b.R * blpha), (int)(a.G * alpha + b.G * blpha), (int)(a.B * alpha + b.B * blpha));
        }

        Color Add(Color a, Color b)
        {
            return Color.FromArgb(Math.Clamp(a.A + b.A, 0, 255), Math.Clamp(a.R + b.R, 0, 255), Math.Clamp(a.G + b.G, 0, 255) , Math.Clamp(a.B + b.B, 0, 255));
        }

        Color Multiply(Color a, Color b)
        {
            return Color.FromArgb((int)(((a.A / 255f) * (b.A / 255f)) * 255), (int)(((a.R / 255f) * (b.R / 255f)) * 255), (int)(((a.G / 255f) * (b.G / 255f)) * 255), (int)(((a.B / 255f) * (b.B / 255f)) * 255));
        }

        unsafe Bitmap ToBitmap(ImageData bitmap)
        {
            fixed (byte* p = bitmap.Data)
            {
                return new Bitmap(bitmap.ImageSize.Width, bitmap.ImageSize.Height, bitmap.Stride, PixelFormat.Format24bppRgb, new IntPtr(p));
            }
        }
    }
}